#include <iostream>
#include <string>
#include <map>

using namespace std;

// use struct if you have POD (plain old data)
// by default, everything is public

struct Analysis{
	int sum;
	int product;
};

// use class if it is something more sophisticated
// by default, everything is private

string get_analysis_str(int a, int b){
	string sum = to_string(a+b);
	string product = to_string(a*b);
	return sum + " " + product;
}

map<string,int> get_analysis_map(int a, int b){
	map<string,int> m;
	m["sum"] = a + b;
	m["product"] = a * b;
	return m;
}

void get_analysis_ref(int a, int b, int& sum, int& prod){
	sum = a + b;
	prod = a * b;
}

Analysis get_analysis(int a, int b){
	Analysis an;
	an.sum = a + b;
	an.product = a * b;
	return an;
}

int main(){
	/*
	 * get_analysis(int a, int b);
	 * return sum of a and b as well as product of a and b
	 */
	//option 1: use a string as return value and put it in string
	//option 2: use a map and return a map with the values mapped out
	// m["sum"], m["product"]
	//option 3: return nothing, but pass parameters by reference
	//int sum, prod;
	//get_analysis_ref(5,3,sum,prod);
	//option 4: use a struct/class

	Analysis a;
	a.sum = 135;
	a.product = 1000;
	cout << a.sum << endl;
	cout << a.product << endl;
	return 0;
}

