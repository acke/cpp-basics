"""
perm(n):
n: integer (non-negative)
returns a vector of all permutations of length n
where the elements are from 1 to n
perm(1): [[1]]
perm(2): [[1,2],[2,1]]
perm(3): [[1,2,3],[1,3,2],[3,1,2],[2,1,3],[2,3,1],[3,2,1]]
"""

def perm(n):
    if n == 0:
        return []
    elif n == 1:
        return [(1,)]
    else:
        prev = perm(n-1)
        ans = []
        for p in prev:
            for i in range(n):
                ans.append(p[:i] + (n,) + p[i:])
        return ans

def perm_yield(n):
    if n == 0:
        yield ()
    elif n == 1:
        yield (1,)
    else:
        prev = perm_yield(n-1)
        for p in prev:
            for i in range(n):
                yield p[:i] + (n,) + p[i:]


from string import ascii_letters, digits

chars = ascii_letters + digits

"""
alphabet: a b c
size 1: a, b, c
size 2: aa, ab, ac, ba, bb, bc, ca, cb, cc
size 3: aaa, aab, aac, aba, abb, abc, aca, acb, acc, ...
"""

def bruteforce(n):
    if n == 0:
        yield ""
    else:
        passwords = bruteforce(n-1)
        for password in passwords:
            for c in chars:
                yield password + c


secret_password = "fork"
def f():
    for n in range(1,5):
        for s in bruteforce(n):
            if s == secret_password:
                print("FOUND PASSWORD: {}".format(s))
                return
            else:
                print(s)

f()

# Write combination(5,2):
# (1,2), (1,3), (1,4), (1,5), (2,3), (2,4), (2,5), (3,4), (3,5), (4,5)

# Imagine you have a rope around the Earth's diameter.
# I add 6 meters to the rope and it is floating like a ring around the Earth
# 1) How far away from the diameter does it rise?
# 2) Imagine we pull the rope at a point away from the Earth. You will sort
# of get a triangle on one end and hugging rope on the other end.
# How high is the tip of the triangle away from the Earth?
# Hint: Use trigonometry (cos, sin, tan), arcs in a circle, Pythagoreum theorem
# Power of a point (could be useful) (you may need to use bisection to solve for
# a variable, it will be difficult to solve algebraically)

