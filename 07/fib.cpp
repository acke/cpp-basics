#include <iostream>
#include <cstring>

using namespace std;

int cache[1000000];

int fib(int n){
	if (n == 0)
		return 0;
	else if (n == 1)
		return 1;
	else if(cache[n] > -1)
		return cache[n];
	else
		return cache[n] = fib(n-1) + fib(n-2);
}

int main(){
	memset(cache, -1, sizeof(cache));
	cout << fib(1000) << endl;
	return 0;
}

