#include <iostream>

using namespace std;

double calc_debt(double d, int m, int v, double i){
	double debt = v;
	while (m--){
		debt = debt*(1+i) - d;
	}
	return debt;
}

int main(){
	// Standard binary search
	// std::lower_bound in algorithms library
	// use this instead of writing your own binary search
	// Used for finding elements in sorted array
	//
	// Bisection Method
	// Took out a loan for a car and you're paying monthly
	// d dollars for m months, car is originally v dollars,
	// bank has interest rate i for any unpaid loan at end of 
	// each month. Find what d should be to pay off everything
	//
	// m = 2, v = 1000, i = 10%, d = 576.19
	// 1000 * 1.1 - 576.19 = 523.81
	// 523.81 * 1.1 - 576.19 ~= 0
	
	// d*(1-(1+i)^{-m})/i = v
	int m = 2, v = 1000;
	double i = 0.1;
	double low, high, mid;
	low = 0.01;
	high = v * (1+i);
	while (high - low > 0.001){
		mid = (high+low)/2.0;
		double debt = calc_debt(mid,m,v,i);
		cout << low << " " << high << " " << mid << " " << debt << endl;
		if (debt > 0){
			low = mid;
		}
		else{
			high = mid;
		}
	}
	cout << mid << endl;

	// UVa 11935 - Through the Desert

	return 0;
}

