import queue

# State representation of the problem
# indices: object
# 0: boat
# 1: feed
# 2: chicken
# 3: wolf
# initial_state = (0, 0, 0, 0)

# input: state: (x,x,x,x)
# output: list of states [(...),(...),...]
def get_neighbors(state):
    boat_loc = state[0]
    neighbors = [(1-boat_loc,)+state[1:]]
    for i in range(1,4):
        if state[i] == boat_loc:
            neigh_state = list(state)
            neigh_state[0] = 1-boat_loc
            neigh_state[i] = 1-state[i]
            neighbors.append(tuple(neigh_state))
    return neighbors

def is_illegal(state):
    return ((state[1] == state[2] and state[0] != state[2]) or 
            (state[2] == state[3] and state[0] != state[2]))

initial_state = (0, 0, 0, 0)
q = queue.Queue()
q.put((initial_state, ()))
visited = set()
while not q.empty():
    state, history = q.get()
    visited.add(state)
    if state == (1, 1, 1, 1):
        for s in history:
            print(s)
        print(state)
        break
    neighbors = get_neighbors(state)
    for neighbor in neighbors:
        if neighbor in visited or is_illegal(neighbor):
            continue
        q.put((neighbor, history + (state,)))

