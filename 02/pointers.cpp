#include <cstdio>



int main(){
	/*
	// pointers are variables that refer to other variables
	int* p; // variable p is a pointer to some integer
	int a = 5; // somewhere in your computer, you stored 5
	// this location has an address that may look something like
	// 0xfff34abc
	printf("%x\n", &a); // & refers to location of a
	p = &a;
	printf("%p\n", p);
	printf("%d\n", *p); // *p dereferences p and shows what is 
	// inside the address it is pointing to
	a = 15;
	printf("%d\n", *p);
	*/

	int arr[10];
	arr[0] = 10;
	printf("%d\n", arr[0]);
	*arr = 15;
	printf("%d\n", arr[0]);
	// arr[1] = 5 <==> *(arr+1) = 5;
	1[arr] = 20;
	// 1[arr] = 20 <==> *(1+arr) = 20;
	
	return 0;
}

