#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

/*
 * pass by reference vs pass by value
 */

// pass by value
// parameters are copies!
int add(int a, int b){
	a = 10;
	return a + b;
}

// pass by reference
// parameters actually are the real thing!
int addr(int& a, int& b){
	a = 10;
	return a + b;
}

// pass by value -> pass by reference
int sum(const vector<int>& v){
	int s = 0;
	for (int i = 0; i < v.size(); i++){
		s += v[i];
	}
	return s;
}

int main(){
	/*
	int a = 5, b = 10;
	int c = addr(a,b);
	printf("a: %d, b: %d, c: %d\n", a, b, c);
	*/
	vector<int> v;
	for (int i = 0; i < 10000000; i++){
		v.push_back(i);
	}
	cout << sum(v) << endl;
	int a = 5;
	int& b = a;
	a = 7;
	cout << b << endl;
}

