#include <iostream>
#include <vector>

using namespace std;

int main(){
	cout << "Enter your favorite numbers. Enter 0 to stop:" << endl;
	vector<int> v;
	int n;
	while (cin >> n){
		if (n == 0)
			break;
		else
			v.push_back(n);
	}
	for (int i = 0; i < v.size(); i++){
		cout << v[i] << " ";
	}
	cout << endl;
	return 0;
}

