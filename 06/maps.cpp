#include <iostream>
#include <map>
#include <string>

using namespace std;

/*
 * http://www.cplusplus.com/reference/map/map/
 */

int main(){
	map<char,int> freq;
	string s = "aegouewfiuwecowerg";
	for (int i = 0; i < s.length(); i++){
		char c = s[i];
		freq[c]++;
	}
	// freq['a'] = 30;
	// 
	/*
	for (int i = 'a'; i <= 'z'; i++){
		char c = (char)i;
		cout << c << ": " << freq[c] << endl;
	}
	*/

	// for key in dict:
	for (map<char,int>::iterator it = freq.begin(); it != freq.end(); it++){
		cout << it->first << " " << it->second << endl;
	}

	return 0;
}

