// Include input output library
#include <iostream>

// To compile program: run
// g++ hello.cpp
// This creates an executable called a.out
// To name the executable: run
// g++ -o hello hello.cpp

using namespace std;
// using std::cout;
// using std::endl;

int main(){
	cout << "Hello World" << endl;
	return 0;
}

