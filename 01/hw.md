# Homework

1. Write a program that takes in a line as input (whole line) for your full
   name.

   ./hello
	What is your name?
	John Doe // input
	Prepare to die, John Doe!

	Hint: use getline

2. Prime checker: write a function that takes in integer as input and outputs a
   boolean of whether it is prime or not

