#include <iostream>
#include <cstring>
#include <vector>
#include <queue>
#include <stack>

using namespace std;
typedef vector<int> vi;

vector<vi> adj_list;
bool visited[5];
int dist[5];

void dfs(int u){
	visited[u] = true;
	cout << "Visited " << u << endl;
	for (int i = 0; i < adj_list[u].size(); i++){
		int v = adj_list[u][i];
		if (!visited[v])
			dfs(v);
	}
}

void bfs(int s){
	queue<int> q;
	dist[s] = 0;
	q.push(s);
	while (!q.empty()){
		int u = q.front();
		q.pop();
		cout << "Visited " << u << endl;
		for (int i = 0; i < adj_list[u].size(); i++){
			int v = adj_list[u][i];
			if (dist[v] == -1)
				dist[v] = dist[u] + 1;
				q.push(v);
			}
		}
	}
}

int main(){
	adj_list.assign(5,vi());
	adj_list[0].push_back(1);
	adj_list[1].push_back(0);
	adj_list[1].push_back(2);
	adj_list[1].push_back(3);
	adj_list[1].push_back(4);
	adj_list[2].push_back(1);
	adj_list[2].push_back(3);
	adj_list[3].push_back(1);
	adj_list[3].push_back(2);
	adj_list[3].push_back(4);
	adj_list[4].push_back(1);
	adj_list[4].push_back(3);
	for (int i = 0; i < adj_list.size(); i++){
		cout << i << ": ";
		for (int j = 0; j < adj_list[i].size(); j++){
			cout << adj_list[i][j] << " ";
		}
		cout << endl;
	}
	memset(visited, 0, sizeof(visited));
	memset(dist, -1, sizeof(dist));
	bfs(3);
	for (int i = 0; i < 5; i++){
		cout << i << ": " << dist[i] << endl;
	}
	return 0;
}

