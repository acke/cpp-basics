#include <iostream>
#include <vector>

using namespace std;

int main(){
	// declare an array
	int a[10];
	a[5] = 5;
	// a[0] is garbage, may or may not be 0
	// almost same as Java
	
	// arraylist of C++
	vector<int> v;
	v.push_back(5);
	v.push_back(10);
	// [5, 10]
	// v[0] == 5
	// v[1] == 10
	// dynamic array (we dont need fix the size when we declare it)
	// C++ documentation

	return 0;
}

