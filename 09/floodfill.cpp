// UVa 469 - Wetlands of Florida

#include <iostream>
#include <vector>

using namespace std;

vector<vector<int>> grid = {
	{0,0,0,0,0,0},
	{0,1,1,1,1,0},
	{0,1,1,1,0,0},
	{0,1,1,1,1,0},
	{1,1,0,0,0,0},
	{0,0,0,0,0,0}
};

const int R = grid.size();
const int C = grid[0].size();

void print_grid(vector<vector<int>>& grid){
	for (int i = 0; i < grid.size(); i++){
		for (int j = 0; j < grid[i].size(); j++){
			cout << grid[i][j];
		}
		cout << endl;
	}
	cout << endl;
}

int dr[] = {1,1,0,-1,-1,-1, 0, 1};
int dc[] = {0,1,1, 1, 0,-1,-1,-1};

int floodfill(int r, int c, int c1, int c2){
	if (r < 0 || r >= R || c < 0 || c >= C){
		return 0;
	}
	if (grid[r][c] != c1){
		return 0;
	}
	int ans = 1;
	grid[r][c] = c2;
	for (int i = 0; i < 8; i++){
		ans += floodfill(r + dr[i], c + dc[i], c1, c2);
	}
	return ans;
}

int main(){
	cout << R << " " << C << endl;
	print_grid(grid);
	floodfill(1,2,1,2);
	print_grid(grid);
	cout << endl;
	return 0;
}

