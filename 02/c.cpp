#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

#define FIVE 5
typedef vector<int> vi;

int crazy_func(){
	return 10;
}

int main(){
	/*
	// instead of cin, scanf
	// instead of cout, printf
	printf("Hello World\n");
	int a = 15;
	int b = 20;
	int c = 35;
	// cout << "a = " << a << ", " << ...
	printf("a = %d, b = %d, c = %d\n", a, b, c);
	*/

	/*
	int arr[100]; // int has 4 bytes
	memset(arr, 0, sizeof(arr)); // sizeof is size of arr in bytes
	// only for zeroing out or for making everything -1
	for (int i = 0; i < 100; i++){
		printf("%d\n", arr[i]);
	}
	*/

	int a = 5;
	int b = (a < 10) ? 7 : 17;
	/*
	if (a < 10)
		b = 7;
	else
		b = 17;
	*/

	if (a == 5 || crazy_func()){
		printf("%d\n", a);
	}

	// binary operators
	// &, |, ^
	int a = 1; //    0001
	int b = 3; //    0011
	int c = a & b;// 0001 // and
	int d = a | b;// 0011 // or
	int e = a ^ b;// 0010 // xor (exclusive or) (add)

	// in C, any integer that is not 0 is consider true
	// 0 is considered false

	return 0;
}

