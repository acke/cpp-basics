#include <iostream>

using namespace std;

int main(){
	// Problem Solving Paradigms
	// 1. Complete Search / Brute force / Recursive Backtracking
	// ex. find all numbers between 0 to 1,000,000 that are divisible by 3 and 5 and contain a 6 in a digit
	// 2. Divide and Conquer
	// ex. binary search - find a certain element in sorted array
	// 3. Greedy
	// ex. solve problem by taking "greedy" choices
	// 4. Dynamic Programming
	// ex. "cache" or "memoization"
	
	// A = [10,7,3,5,8,2,9], n = 7
	// 1. Find largest and smallest element of A (10, 2)
	// 2. Find the kth smallest element in A (if k = 2, 3)
	// 3. Find the largest gap g such that x,y in A and g = |x-y| (8)
	// 4. Find the longest increasing subsequence in A. ({3,5,8,9})
	//
	// Iterative Complete Search
	// UVa 725 - Division
	// Find all pairs of 5-digit numbers that all use the digits 0 to 9 once each together, such that the
	// first number divided by the second is equal to an integer where 2 <= N <= 79, abcde/fghij = N where 
	// each letter is a different digit, the first digit is allowed to be 0
	// N = 62, 79546 / 01283 = 62, 94736 / 01528 = 62
	// data structure : set - like a map, but only keys (no values)
	// set is unordered (no s[5]), checking if something is in s is fast
	// also no duplicates are allowed in sets (so you can insert every digit into the set and check if you
	// have 10 elements in the end)
	//
	// UVa 441 - Lotto
	// given 6 < k < 13, enumerate all subsets of size 6 in sorted order (input is k sorted integers)
	// 1 2 3 4 5 6 7 8 9 10 11 12
	// 1 2 3 4 5 6, 1 2 3 4 5 7, 1 2 3 4 5 8, ... 12_C_6 = 924
	// use 6 nested for loops
	//
	// UVa 11565 - Simple Equations
	// Given three integers A, B, C (1 <= A,B,C <= 10000), find x, y, z such that x+y+z = A,
	// x * y * z = B, x^2 + y^2 + z^2 = C
	// consider triple for loop over x y z (10000*10000*10000 = 10^12 too slow)
	// consider "pruning" - cut out some of the brute force
	//
	// UVa 11742 - Social Constraints
	// 0 <= n <= 8 movie goers. Sit in front row in n consecutive open seats
	// There are 0 <= m <= 20 seating constraints i.e. a and b must be at least (at most) c seats apart
	// How many possible seating arrangements are there?
	//
	// Given numbers (1,2,3):
	// 1 2 3, 1 3 2, 2 1 3, 2 3 1, 3 1 2, 3 2 1
	// next_permutation function in C++ #include <algorithm>
	return 0;
}

