#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
	//int arr[5] = {2,5,4,3,1};
	// find the max elt
	// arr: size N : N
	// sorting: N log N
	// 1) Linear Scanning: runs in N time
	// Good for unordered arrays
	//int arr[5] = {1,2,3,4,5};
	// assume arr was already sorted
	// max : time 1
	// median : time 1
	// search for a number: 13613
	// size was N=10000000
	// unordered: time N
	// ordered: binary search
	// time log N
	// 2) Binary search: runs in log N time
	// 3) Hashing / direct addressing
	// a big string: track occurences of each character
	// characters: a - z
	
	// vector<int> v;
	// sort(v.begin(), v.end());
	// int arr[size];
	// sort(arr, arr+size);
	int arr[26];
	memset(arr, 0, sizeof arr);
	// found a letter a
	char c = 'a';
	arr[c - 'a']++;
	cout << (int)c << endl;
	return 0;
}

