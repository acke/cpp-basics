# Notes

Python 
- interpreted language: the computer reads through the code line by line and
  runs it as it goes 

C++ 
- compiled language: a "compiler" reads through the code first, converts it into
machine language that is prob more optimized, which you can then run separately

