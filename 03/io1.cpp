#include <cstdio>
#include <iostream>

using namespace std;

int main(){
	/*
	 * Two numbers in a line
	 * keep reading until they are both 0 0
	 */
	int a, b;
	while (true) {
		cin >> a >> b;
		// scanf("%d %d", &a, &b);
		if (a == 0 && b == 0)
			break;
		//cout << a << " + " << b << " = " << a + b << endl;
		printf("%d + %d = %d\n", a, b, a+b);
	}
	return 0;
}

