# How fast can you type?
# This is one metric that usually doesn't matter, but
# can make an impact if you solve faster than you can type
# If below 50 words per minutes, do typing practice tests

# Identifying what kind of problem it is
# Ad Hoc
# Complete Search (iterative/recursive)
# Divide and Conquer
# Greedy
# Dynamic Programming
# Graph
# Math
# String-based
# Computational Geometry
# Miscellaneous hard topics

# I have solved this before (and can re-solve fast)
# I have seen this before (but not sure how to solve confidently)
# I have not seen before (then youre screwed)

# Algorithm Analysis
# Given input bound, does my solution work in time?
# Ex. find the pair of numbers in a list with max abs difference
# Idea 1) Brute force through all pairs and keep track of max
# Given n numbers, ~n^2
# If n = 1,000,000, too slow
# If n = 10,000, acceptable
# Idea 2) Sort and take first and last
# Given numbers, ~n log n
# Good up to n = 1,000,000
# Idea 3) Max - min
# ~n
# Good up to ~100,000,000

# Simple strategies are best if they work in time
# 2^10 = 1024 ~= 10^3
# 2^20 = 1,048,576 ~= 10^6
# n! : <= 11
# 2^n * n : <= 20-ish
# n^2 : <= 10,000

# I/O Routines
# how to read the following:
# number of test cases in first line
# multiple test cases with last one being zeroes
# (not as common) test cases until end of file

# ex. first line is n, number of test cases
# ex. next n lines are 3 numbers, output "Test case #{i}: {sum}"
def ex1():
    n = int(input())
    for i in range(n):
        nums = [int(i) for i in input().split()]
        print("Test Case #{}: {}".format(i+1, sum(nums)))

# Quick Ad-Hoc exercises
# 00489 - Hangman Judge
# 10189 - Minesweeper
# 11459 - Snakes and Ladders

# Data Structures (time to append, delete, sort, pop, etc.)
# Lists / arrays
# Dictionaries / maps
# Sets (dictionaries with only keys)
# Stacks / Queues
# Double ended queue
# Heaps (important, for future)
# Hash tables (dictionaries/sets are actually hash tables underneath)
# Graphs (adjacency list)

# Quick Quiz:
# time to run following codes
# list(range(n)) + [1]
# [1] + list(range(n))
# how long does this take? 
# time n

# l.append(whatever)
# this is constant

# solve
# 467 - Synching Flags
# 591 - Box of Bricks
# 12187 - Brothers (simulate)
# 10880 - Colin and Ryan (use sort)
# 10282 - Babelfish (dictionaries)
# 10646 - What is the Card? (card simulation)
# 278 - Chess (possible to do with math)
# 401 - Palindromes
# 156 - Anagram (use sort)
# 10812 - Beat the Spread (careful of edge case)


