USACO December 2014 Bronze
Marathon
Crosswords
Cow Jog
Learning by Example


Recursion Practice
Factorial - fac(n) = n!
Exponentiation - pow(b, e) = b^e

Fibonacci - fib(n) = fib(n-1) + fib(n-2)
fib(0) = 0, fib(1) = 1

Naive Fibonacci - try to solve fib(40)
Optimized Fibonacci using caching
use maps (if an answer has been seen before, just return it directly)

map<int,int> cache;
once solving a fib problem fib(k), save it in cache and if fib(k) is called again, then return cache[k]



