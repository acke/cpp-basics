def qs(arr):
    if len(arr) <= 1:
        return arr.copy()
    x = arr[0]
    return qs([i for i in arr if i < x]) + [x] + qs([i for i in arr if i > x])

arr = [5,1,2,8,3,7,6,0]
print(qs(arr))

