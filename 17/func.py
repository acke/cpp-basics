def accum(arr, foo):
    total = arr[0]
    for val in arr[1:]:
        total = foo(val,total)
    return total

def sums(a,b):
    return a + b

def prods(a,b):
    return a * b

arr = [1,2,3,4,5]
print(accum(arr,sums))
print(accum(arr,prods))

