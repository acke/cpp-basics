#include <iostream>
#include <set>

using namespace std;

int main(){
	// abcde/fghij = N where each letter is unique
	// fghij * N = abcde
	// 01234 * 62 = 76508
	int N = 62;
	for (int fghij = 1234; fghij <= 98765; fghij++){
		int abcde = fghij * N;
		set<int> s;
		if (fghij < 10000){
			s.insert(0);
		}
		int tmp = fghij;
		while (tmp > 0){
			s.insert(tmp % 10);
			tmp /= 10;
		}
		tmp = abcde;
		while (tmp > 0){
			s.insert(tmp % 10);
			tmp /= 10;
		}
		if (s.size() == 10){
			printf("%d / %d = %d\n", abcde, fghij, N);
		}
	}
	return 0;
}

