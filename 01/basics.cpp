#include <iostream>
#include <string>

// same as Java
// type func_name(arg){ }

// to call: ex: add(5,7) should return 12
int add(int a, int b){
	return a + b;
}

int main(){
	// Variables
	int i = 5;
	float j = 3.57;

	// long in Java
	long long k = 13524634643;

	bool b1 = true;
	bool b2 = false;

	string s = "Hello";

	// Conditionals
	if (i == 5 && b1 || b2){
		cout << s << endl;
	}

	// Loops
	while (true){
		// do whatever
		break;
	}

	for (int c = 0; c < 10; c++){
		cout << c << endl;
	}

	return 0;
}

