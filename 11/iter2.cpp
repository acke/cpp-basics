#include <iostream>

using namespace std;

int main(){
	// 1 2 3 4 5
	// print out all triples in order
	for (int i = 1; i <= 3; i++){
		for (int j = i+1; j <= 4; j++){
			for (int k = j+1; k <= 5; k++){
				printf("%d %d %d\n", i, j, k);
			}
		}
	}
	return 0;
}

