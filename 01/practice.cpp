#include <iostream>
#include <algorithm>

// tell me size of array
// give me list of numbers
// min, max, average, median, range

using namespace std;

int main(){
	cout << "Size: " << endl;
	int size;
	cin >> size;
	cout << "Type in list of numbers: " << endl;
	int arr[size];
	for (int i = 0; i < size; i++){
		cin >> arr[i];
	}
	int min = arr[0], max = arr[0];
	int avg = 0;
	for (int i = 0; i < size; i++){	
		if (min > arr[i])
			min = arr[i];
		if (max < arr[i])
			max = arr[i];
		avg += arr[i];
	}
	avg /= size;
	sort(arr, arr+size);
	int med = arr[size/2];
	if (size % 2 == 0){
		med += arr[size/2 - 1];
		med /= 2;
	}
	cout << "Min: " << min << endl;
	cout << "Max: " << max << endl;
	cout << "Avg: " << avg << endl;
	cout << "Med: " << med << endl;
	cout << "Range: " << max-min << endl;

	return 0;
}

